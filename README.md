# ApoA1 benchmark

The project contains ApoA1 benchmark files for the performance analysis. ApoA1 is a protein-lipid system in water that is a popular example of a biomolecular MD system consisting of 92224   atoms (e.g. citation). We use the [CHARMM36 force field](https://www.charmm.org/charmm/resources/charmm-force-fields/) with the cut-off radius for non-bonded interactions of 1.2 nm and the relative accuracy 0.0005 for electrostatic forces (PME/PPPM). The timestep of integration of equations of motion from atoms is 1 fs.

![Test Image 2](ApoA1.png)

In our paper we show the match of forces and energies between the LAMMPS and GROMACS benchmarks which confirms the equivalence of the initial configurations.

## Running the LAMMPS benchmark

cd lammps

mpirun -np 4 lmp_mpi -in apoa1_36.in -sf gpu -pk gpu 1

## Running the GROMACS benchmark

cd gromacs

gmx_mpi grompp -f run.mdp -c apoa1_36.gro -p apoa1_36.top -o run.tpr -maxwarn 1 

mpirun -np 4 gmx_mpi mdrun -s run.tpr

## Authors

* **Nikolay Kondratyuk** - *Conversion of NAMD benchmark for use in LAMMPS and GROMACS*
* **Vladimir Stegailov** - *Scientific advisor*

## Acknowledgments

* [Pieter J. in’t Veld](https://www.researchgate.net/profile/Pieter_In_t_Veld) and Paul Crozier for the *ch2lmp* tool
* [Visual Molecular Dynamics](https://www.ks.uiuc.edu/Research/vmd/)
